# Exercise guide

## Docker database creation

In order to lunch de postgres docker database, open a console and go into the docker folder.<br/>
Then type the command : ```docker-compose -p movie_exercise up -d```<br/><br/>
Informations on the database :
* user : postgres
* password : postgres
* database name : movie
* port : 5432

## Summary

The goal of this exercise is to create a batch and a webservice using spring-boot.<br/>
You'll have to import the datas of a json file into the database with the batch and query them with the webservice.

## Configuration
* java : openjdk 11
* maven : 3.9.0
* spring boot : 2.7.9

## The library

You first have to create a spring boot library which will contain all the database related classes (entity and
repository)
Make a test class for the repository working with a h2 database (only for tests).

## The batch

Create a spring boot batch application which will have to import datas of the json file movies.json into the database.<br/>
Use a reader, a processor and a writer in order to do so.<br/>
Look at the columns in the database to see which data in the json you should keep.