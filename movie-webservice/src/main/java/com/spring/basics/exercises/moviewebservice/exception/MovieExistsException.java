package com.spring.basics.exercises.moviewebservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MovieExistsException extends RuntimeException {
    public MovieExistsException(long movieId) {
        super("A movie with the id : " + movieId + " already exists");
    }
}
