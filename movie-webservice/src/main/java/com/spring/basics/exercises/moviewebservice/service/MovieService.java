package com.spring.basics.exercises.moviewebservice.service;

import com.spring.basics.exercises.moviecommon.entity.Movie;
import com.spring.basics.exercises.moviecommon.repository.MovieRepository;
import com.spring.basics.exercises.moviewebservice.dto.MovieDto;
import com.spring.basics.exercises.moviewebservice.dto.mapper.MovieMapper;
import com.spring.basics.exercises.moviewebservice.exception.MovieExistsException;
import com.spring.basics.exercises.moviewebservice.exception.NoMovieException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MovieService {

    private MovieRepository movieRepository;
    private MovieMapper movieMapper;

    public List<MovieDto> getAllMovie() {
        List<Movie> movieList = this.movieRepository.findAll();
        return this.movieMapper.toDtoList(movieList);
    }

    public MovieDto getMovieById(Long id) {
        Movie movie = this.movieRepository.findById(id).orElseThrow(() -> new NoMovieException(id));
        return this.movieMapper.toDto(movie);
    }

    public long createMovie(final MovieDto movieDto) {
        Optional<Movie> dbMovie = this.movieRepository.findByTitle(movieDto.getTitle());

        if (dbMovie.isPresent()) {
            throw new MovieExistsException(dbMovie.get().getMovieId());
        }

        Movie movie = this.movieMapper.toEntity(movieDto);

        return this.movieRepository.save(movie).getMovieId();
    }

    public MovieDto updateMovie(Long id, final MovieDto movieDto) {
        Movie existingUser = this.movieRepository.findById(id)
                .map(dbMovie -> {
                    Movie movie = this.movieMapper.toEntity(movieDto);
                    movie.setMovieId(id);
                    return this.movieRepository.save(movie);
                })
                .orElseThrow(() -> new NoMovieException(id));

        return this.movieMapper.toDto(existingUser);
    }

    public void deleteMovie(Long id) {
        this.movieRepository.findById(id)
                .map(dbMovie -> {
                    this.movieRepository.deleteById(dbMovie.getMovieId());
                    return dbMovie;
                })
                .orElseThrow(() -> new NoMovieException(id));
    }
}
