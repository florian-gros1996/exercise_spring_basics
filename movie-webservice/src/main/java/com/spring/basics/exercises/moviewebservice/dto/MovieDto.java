package com.spring.basics.exercises.moviewebservice.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class MovieDto {
    /**
     * Title of the movie
     */
    private String title;

    /**
     * Budget spent to realize the movie
     */
    private int budget;

    /**
     * Release date of the movie
     */
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate release;

    /**
     * Genre of the movie
     */
    private String genre;

    /**
     * Rating of the movie by imdb
     */
    private double imdbRating;

    /**
     * Number of vote in the imdb rating
     */
    private int imdbVotes;
}
