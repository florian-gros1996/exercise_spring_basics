package com.spring.basics.exercises.moviewebservice.dto.mapper;

import com.spring.basics.exercises.moviecommon.entity.Movie;
import com.spring.basics.exercises.moviewebservice.dto.MovieDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MovieMapper {

    Movie toEntity(MovieDto movieDto);

    MovieDto toDto(Movie movie);

    List<MovieDto> toDtoList(List<Movie> movieList);
}
