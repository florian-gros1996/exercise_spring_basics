package com.spring.basics.exercises.moviewebservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieWebserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MovieWebserviceApplication.class, args);
    }

}
