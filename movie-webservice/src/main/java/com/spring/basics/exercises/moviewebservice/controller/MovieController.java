package com.spring.basics.exercises.moviewebservice.controller;

import com.spring.basics.exercises.moviewebservice.dto.MovieDto;
import com.spring.basics.exercises.moviewebservice.service.MovieService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/movies")
@AllArgsConstructor
public class MovieController {

    private MovieService movieService;

    @GetMapping
    public ResponseEntity<List<MovieDto>> getAllMovie() {
        List<MovieDto> movieList = this.movieService.getAllMovie();
        return ResponseEntity.ok(movieList);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MovieDto> getMovieById(@PathVariable Long id) {
        MovieDto movie = this.movieService.getMovieById(id);
        return ResponseEntity.ok(movie);
    }

    @PostMapping
    public ResponseEntity<Long> createMovie(@RequestBody MovieDto movie) {
        Long idUserCreated = this.movieService.createMovie(movie);
        return new ResponseEntity<>(idUserCreated, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<MovieDto> updateMovie(@PathVariable Long id, @RequestBody MovieDto movie) {
        MovieDto movieUpdated = this.movieService.updateMovie(id, movie);
        return ResponseEntity.ok(movieUpdated);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMovie(@PathVariable Long id) {
        this.movieService.deleteMovie(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
