package com.spring.basics.exercises.moviewebservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NoMovieException extends RuntimeException {
    public NoMovieException(long movieId) {
        super("No movie with the id : " + movieId);
    }
}
