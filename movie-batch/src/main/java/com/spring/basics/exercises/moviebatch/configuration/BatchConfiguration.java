package com.spring.basics.exercises.moviebatch.configuration;

import com.spring.basics.exercises.moviebatch.dto.MovieDto;
import com.spring.basics.exercises.moviebatch.listener.BatchJobListener;
import com.spring.basics.exercises.moviebatch.processor.MovieProcessor;
import com.spring.basics.exercises.moviecommon.entity.Movie;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.database.builder.JpaItemWriterBuilder;
import org.springframework.batch.item.json.JacksonJsonObjectReader;
import org.springframework.batch.item.json.JsonItemReader;
import org.springframework.batch.item.json.builder.JsonItemReaderBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.persistence.EntityManagerFactory;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final EntityManagerFactory entityManagerFactory;

    private final MovieProcessor movieProcessor;

    @Value("${application.import.filePath}")
    private String jsonMovieFileName;

    public BatchConfiguration(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory, EntityManagerFactory entityManagerFactory, MovieProcessor movieProcessor) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.entityManagerFactory = entityManagerFactory;
        this.movieProcessor = movieProcessor;
    }

    public JsonItemReader<MovieDto> jsonMovieItemReader() {
        return new JsonItemReaderBuilder<MovieDto>()
                .jsonObjectReader(new JacksonJsonObjectReader<>(MovieDto.class))
                .resource(new ClassPathResource(this.jsonMovieFileName))
                .name("jsonMovieItemReader")
                .build();
    }

    public JpaItemWriter<Movie> movieItemWriter() {
        return new JpaItemWriterBuilder<Movie>()
                .entityManagerFactory(entityManagerFactory)
                .build();
    }

    @Bean("importJsonMovieStep")
    public Step importJsonMovieStep() {
        return stepBuilderFactory.get("importJsonMovieStep")
                .<MovieDto, Movie>chunk(10)
                .reader(jsonMovieItemReader())
                .processor(this.movieProcessor)
                .writer(movieItemWriter())
                .build();
    }

    @Bean
    public Job importMovieJob(@Qualifier("importJsonMovieStep") Step importJsonMovieStep) {
        return jobBuilderFactory.get("importMovieJob")
                .incrementer(new RunIdIncrementer())
                .listener(new BatchJobListener())
                .start(importJsonMovieStep)
                .build();
    }


}
