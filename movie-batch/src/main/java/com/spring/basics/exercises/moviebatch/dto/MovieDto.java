package com.spring.basics.exercises.moviebatch.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class MovieDto {

    /**
     * Title of the movie
     */
    @JsonProperty("Title")
    private String title;

    /**
     * Budget spent to realize the movie
     */
    @JsonProperty("Production Budget")
    private int budget;

    /**
     * Release date of the movie
     */
    @JsonProperty("Release Date")
    private String release;

    /**
     * Genre of the movie
     */
    @JsonProperty("Major Genre")
    private String genre;

    /**
     * Rating of the movie by imdb
     */
    @JsonProperty("IMDB Rating")
    private double imdbRating;

    /**
     * Number of vote in the imdb rating
     */
    @JsonProperty("IMDB Votes")
    private int imdbVotes;
}
