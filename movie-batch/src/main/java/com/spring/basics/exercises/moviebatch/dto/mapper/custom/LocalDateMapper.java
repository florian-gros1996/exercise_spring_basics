package com.spring.basics.exercises.moviebatch.dto.mapper.custom;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Component
public class LocalDateMapper {

    private final static String DATE_FORMAT = "MMM dd yyyy";

    public String asString(LocalDate date) {
        return date != null ? date.format(DateTimeFormatter.ofPattern(DATE_FORMAT, Locale.US)) : null;
    }

    public LocalDate asDate(String date) {
        return date != null ? LocalDate.parse(date, DateTimeFormatter.ofPattern(DATE_FORMAT, Locale.US)) : null;
    }
}
