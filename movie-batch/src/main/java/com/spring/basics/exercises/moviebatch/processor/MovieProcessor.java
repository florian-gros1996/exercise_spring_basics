package com.spring.basics.exercises.moviebatch.processor;

import com.spring.basics.exercises.moviebatch.dto.MovieDto;
import com.spring.basics.exercises.moviebatch.dto.mapper.MovieMapper;
import com.spring.basics.exercises.moviecommon.entity.Movie;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;


@Component
public class MovieProcessor implements ItemProcessor<MovieDto, Movie> {

    private final MovieMapper movieMapper;

    public MovieProcessor(MovieMapper movieMapper) {
        this.movieMapper = movieMapper;
    }

    @Override
    public Movie process(MovieDto movieDto) {
        //Skip the movie if the title is not set
        if (movieDto.getTitle() == null)
            return null;
        return this.movieMapper.toEntity(movieDto);
    }
}
