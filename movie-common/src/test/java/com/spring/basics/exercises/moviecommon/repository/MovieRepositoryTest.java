package com.spring.basics.exercises.moviecommon.repository;

import com.spring.basics.exercises.moviecommon.entity.Movie;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Test the movie repository
 */
@SpringBootTest
@Transactional
class MovieRepositoryTest {

    private final Movie movie;
    @Autowired
    private MovieRepository movieRepository;

    /**
     * init the movie entity used in tests
     */
    public MovieRepositoryTest() {
        EasyRandom generator = new EasyRandom();
        this.movie = generator.nextObject(Movie.class);
    }

    /**
     * Test that the insertion in the db is working
     */
    @Test
    void insertionTest() {
        this.movieRepository.save(this.movie);

        List<Movie> movieList = this.movieRepository.findAll();

        assertFalse(movieList.isEmpty());
        assertEquals(movieList.size(), 1);
    }

    /**
     * Test that updating in the db is working
     */
    @Test
    void UpdatingTest() {
        this.movieRepository.save(this.movie);
        List<Movie> movieList = this.movieRepository.findAll();

        assertFalse(movieList.isEmpty());
        assertEquals(movieList.size(), 1);

        Movie movieToUpdate = movieList.get(0);
        final String genre = "Documentary";
        movieToUpdate.setGenre(genre);
        this.movieRepository.save(movieToUpdate);

        assertFalse(movieList.isEmpty());
        assertEquals(movieList.size(), 1);
        assertEquals(movieList.get(0).getGenre(), genre);
    }

    /**
     * Test that deletion in the db is working
     */
    @Test
    void deletionTest() {
        this.movieRepository.save(this.movie);
        List<Movie> movieList = this.movieRepository.findAll();

        assertFalse(movieList.isEmpty());
        assertEquals(movieList.size(), 1);

        Movie insertedMovie = movieList.get(0);
        this.movieRepository.deleteById(insertedMovie.getMovieId());

        movieList = this.movieRepository.findAll();

        assertTrue(movieList.isEmpty());
    }

}