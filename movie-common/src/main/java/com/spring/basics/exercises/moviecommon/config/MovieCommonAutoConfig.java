package com.spring.basics.exercises.moviecommon.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan("com.spring.basics.exercises.moviecommon.entity")
@EnableJpaRepositories("com.spring.basics.exercises.moviecommon.repository")
public class MovieCommonAutoConfig {
}
