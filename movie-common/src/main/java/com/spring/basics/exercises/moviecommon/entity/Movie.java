package com.spring.basics.exercises.moviecommon.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Entity class representing the movie table
 */
@Entity
@Table(name = "movie", schema = "public")
@Getter
@Setter
@NoArgsConstructor
public class Movie {

    /**
     * id of the movie
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private long movieId;

    /**
     * Title of the movie
     */
    private String title;

    /**
     * Budget spent to realize the movie
     */
    private int budget;

    /**
     * Release date of the movie
     */
    private LocalDate release;

    /**
     * Genre of the movie
     */
    private String genre;

    /**
     * Rating of the movie by imdb
     */
    @Column(name = "imdb_rating", columnDefinition = "NUMERIC(2,1)")
    private double imdbRating;

    /**
     * Number of vote in the imdb rating
     */
    @Column(name = "imdb_votes")
    private int imdbVotes;
}
