package com.spring.basics.exercises.moviecommon.repository;

import com.spring.basics.exercises.moviecommon.entity.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

    Optional<Movie> findByTitle(String movieTitle);
}
